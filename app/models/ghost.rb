class Ghost < ActiveRecord::Base
  attr_accessible :email

  has_many :trades, as: :friend
end
