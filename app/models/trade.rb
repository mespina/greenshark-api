class Trade < ActiveRecord::Base
  belongs_to :user
  belongs_to :friend, :polymorphic => true
  attr_accessible :amount, :trade_type, :friend_email
  attr_accessor :friend_email

  TRADE_TYPES = ['income', 'outcome']

  validates_presence_of :amount, :trade_type, :friend_email
  validates :trade_type, :inclusion => TRADE_TYPES

  before_create :set_friend!

  private
  def set_friend!
  	if user = User.find_by_email(friend_email)
  		self.friend = user
  	else 
  		ghost = Ghost.find_or_create_by_email(friend_email)
  		self.friend = ghost
  	end
  end

end