Greenshark::Application.routes.draw do

  devise_for :users, controllers: {registrations: 'api/v1/registrations'}

  namespace :api do
    namespace :v1 do
      resources :tokens, :only => [:create, :destroy]
      resources :trades
    end
  end

end