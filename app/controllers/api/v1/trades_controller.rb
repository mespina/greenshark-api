class Api::V1::TradesController < ApplicationController
  before_filter :authenticate_user!
  respond_to :json

  def index
    trades = current_user.trades

    respond_with trades
  end

  def create
    trade = current_user.trades.build params[:trade]

    if trade.save
      render status: 200, json: trade
    else
      render status: 401, json: trade.errors
    end
  end

  def destroy
    trade = current_user.trade.find params[:id]

    if trade.destroy
      respond_with 'ok'
    else
      render status: 401, json: trade.errors
    end
  end
end
