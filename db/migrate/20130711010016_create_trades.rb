class CreateTrades < ActiveRecord::Migration
  def change
    create_table :trades do |t|
      t.string :trade_type
      t.float :amount
      t.references :user
      t.integer :friend_id
      t.string :friend_type

      t.timestamps
    end
    add_index :trades, :user_id
    add_index :trades, [:friend_id, :friend_type]
  end
end
