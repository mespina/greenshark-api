class Api::V1::RegistrationsController < Devise::RegistrationsController
  def create
    build_resource params[:user]

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        respond_to do |format|
          format.json { render json: resource }
        end
      else
        expire_session_data_after_sign_in!

        respond_to do |format|
          format.json { render json: resource }
        end
      end
    else
      clean_up_passwords resource
      respond_to do |format|
        format.json { render json: resource.errors }
      end
    end
  end
end